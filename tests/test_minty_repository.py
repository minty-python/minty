# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs import Event, UserInfo
from minty.repository import Repository, RepositoryBase
from unittest import mock
from uuid import uuid4


class MockRepositoryBase(RepositoryBase):
    pass


class TestRepositoryBase:
    def test_repository_base(self):
        infra_factory = mock.MagicMock()
        infra_factory.get_infrastructure.return_value = "bar"

        infra_factory_ro = mock.MagicMock()
        infra_factory_ro.get_infrastructure.return_value = "baz"

        r = MockRepositoryBase(
            infrastructure_factory=infra_factory,
            infrastructure_factory_ro=infra_factory_ro,
            context="c",
            event_service="es",
            read_only=False,
        )

        rv = r._get_infrastructure("foo")
        assert rv == "bar"

        infra_factory.get_infrastructure.assert_called_once_with(
            context="c", infrastructure_name="foo"
        )

        r.read_only = True

        rv = r._get_infrastructure("foo")
        assert rv == "baz"

        infra_factory_ro.get_infrastructure.assert_called_once_with(
            context="c", infrastructure_name="foo"
        )


class MockRepository(Repository):
    _for_entity = "Mock"
    _events_to_calls = {"CaseCreated": "_run_on_created_event"}

    ran = False
    user_info = None
    dry_run = False
    times_ran = 0

    def _run_on_created_event(self, event, user_info, dry_run):
        self.ran = True
        self.user_info = user_info
        self.dry_run = dry_run
        self.times_ran = self.times_ran + 1


class TestRepository:
    def test_save(self):
        event_service = mock.MagicMock()
        event = mock.MagicMock()
        event.event_name = "CaseCreated"
        event.processed = False
        event_service.get_events_by_type.return_value = [event]

        infra_factory = mock.MagicMock()
        infra_factory_ro = mock.MagicMock()
        repo = MockRepository(
            infrastructure_factory=infra_factory,
            infrastructure_factory_ro=infra_factory_ro,
            context="dummy",
            event_service=event_service,
            read_only=True,
        )
        user_info = UserInfo(user_uuid=uuid4(), permissions={"admin": True})

        assert not repo.ran

        repo.save()

        assert repo.ran
        assert repo.user_info is None

        # Reset for another try:
        event.processed = False

        # Save called with user_info
        repo.save(user_info=user_info)
        assert repo.user_info == user_info

    def test_save_with_dry_run(self):
        event_service = mock.MagicMock()
        event = mock.MagicMock()
        event.event_name = "CaseCreated"
        event.processed = False
        event_service.get_events_by_type.return_value = [event]

        infra_factory = mock.MagicMock()
        infra_factory_ro = mock.MagicMock()
        repo = MockRepository(
            infrastructure_factory=infra_factory,
            infrastructure_factory_ro=infra_factory_ro,
            context="dummy",
            event_service=event_service,
            read_only=False,
        )
        assert not repo.ran

        repo.save()
        assert repo.ran
        assert repo.dry_run is False

        # Reset for another try
        event.processed = False

        # Save called with dry_run
        dry_run = True
        repo.save(dry_run=dry_run)
        assert repo.dry_run == dry_run

    def test_save_event_processed(self):
        event_service = mock.Mock()
        user_uuid = uuid4()
        # event = mock.MagicMock()
        event = Event(
            uuid=uuid4(),
            created_date=None,
            correlation_id=uuid4(),
            domain="mydomain",
            context="dummy",
            user_uuid=user_uuid,
            user_info=UserInfo(user_uuid=user_uuid, permissions={}),
            entity_type="case",
            entity_id=uuid4(),
            event_name="CaseCreated",
            changes=None,
            entity_data={},
            processed=False,
        )

        event_service.get_events_by_type.return_value = [event]

        infra_factory = mock.MagicMock()
        infra_factory_ro = mock.MagicMock()
        repo = MockRepository(
            infrastructure_factory=infra_factory,
            infrastructure_factory_ro=infra_factory_ro,
            context="dummy",
            event_service=event_service,
            read_only=False,
        )

        assert not repo.ran
        assert repo.times_ran == 0
        repo.save()
        assert repo.times_ran == 1
        assert event.processed is True

        repo.save()
        assert repo.times_ran == 1
